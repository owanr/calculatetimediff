var main = function() {
    // Put time difference from Minneapolis in associative array (How to do this efficiently?)
    var timeDiffs = new Array();
    timeDiffs["Hawaii"] = -4;
    timeDiffs["Hong Kong"] = 14;
    timeDiffs["New York"] = 1;
    timeDiffs["Minneapolis"] = 0;
    timeDiffs["Moscow"] = 9;
    timeDiffs["San Francisco"] = -2;
    timeDiffs["Seoul"] = 15;
    timeDiffs["Tokyo"] = 15;
    //localStorage.setItem('timeDiffs', JSON.stringify(timeDiffs));
    var weekdayArray = new Array();
    weekdayArray[0] = "Sunday";
    weekdayArray[1] = "Monday";
    weekdayArray[2] = "Tuesday";
    weekdayArray[3] = "Wednesday";
    weekdayArray[4] = "Thursday";
    weekdayArray[5] = "Friday";
    weekdayArray[6] = "Saturday";
    weekdayArray["Sunday"] = 0;
    weekdayArray["Monday"] = 1;
    weekdayArray["Tuesday"] = 2;
    weekdayArray["Wednesday"] = 3;
    weekdayArray["Thursday"] = 4;
    weekdayArray["Friday"] = 5;
    weekdayArray["Saturday"] = 6;
    

    //When initially getting things at the menu page
    $("input[name=gotCity]").click(function () {
        // "Your" information
        var enteredParticipants = [];
        var yourCity = document.getElementById("yourCity").value;
        var yourName = document.getElementById("yourName").value;
        if (yourCity == "None") {
            alert("Please enter your city!");
            return;
        }
        if (yourName == "") {
            yourName = "You";
        }
        var thisParticipant = {thisId: "p0", thisName: yourName, thisCity: yourCity};
        enteredParticipants.push(thisParticipant);
        
        // Get entered available times
        var times = document.getElementById("timeList");
        var timeListItem = times.getElementsByTagName("li");
        var enteredTimes = [];
        for (var i=0; i < timeListItem.length; i++) {
            var startEnd = timeListItem[i].getElementsByTagName( "select" );
            var startTime = startEnd[0].options[ startEnd[0].selectedIndex ].value;
            var endTime = startEnd[1].options[ startEnd[1].selectedIndex ].value;
            if (startTime != "None") {
                if (endTime != "None") {
                    startTime = parseInt(startTime);
                    endTime = parseInt(endTime);
                    // Has both starting and ending time
                    if (startTime > endTime) {
                        alert("Starting time is later than ending time for time " + (i+1) + "!");
                        return;
                    }
                    // Get weekday
                    var week = [];
                    var checked = times.getElementsByClassName("checkbox" + (i+1));
                    var hasWeekday = false;
                    for (var j=0; j < checked.length; j++) {
                        if (checked[j].checked == true) {
                            week.push(checked[j].id);
                            hasWeekday = true;
                        }
                    }
                    if (hasWeekday == false) {
                        alert("Please check at least one weekday for time " + (i+1) + "!");
                        return;
                    }
                    var thisTime = {availWeek: week, startT: startTime, endT: endTime};
                    enteredTimes.push(thisTime);
                } else {
                    // Has starting time but no ending time
                    alert("Please enter ending time for time " + (i+1) + "!");
                    return;
                }
            } else {
                if (endTime != "None") {
                    // Has ending time but no starting time
                    alert("Please enter starting time for time " + (i+1) + "!");
                    return;
                }
            }
        }
        if (enteredTimes.length == 0) {
            alert("You didn't choose anything!");
            return;
        }

        // Get participant info
        // Why didn't name work? Using class name instead
        var participants = document.getElementById("participantList");
        var participantsItem = participants.getElementsByTagName("li");
        for (var i=0; i < participantsItem.length; i++) {
            var cities = participantsItem[i].getElementsByTagName( "select" );
            var chosenCity = cities[0].options[ cities[0].selectedIndex ].value;
            var name = participantsItem[i].getElementsByClassName("nameText")[0].value;
            if (chosenCity != "None") {
                var pid = participantsItem[i].getAttribute('id');  
                if (name == "") {
                    name = pid;
                }
                
                thisParticipant = {thisId: pid, thisName: name, thisCity: chosenCity};
                enteredParticipants.push(thisParticipant);
            } else if (name != "") {
                alert("Please enter " + name + "'s city!");
                return;
            }
        }
        if (enteredParticipants.length == 1) {
            alert("Please enter information for at least one other person!");
            return;
        }
        window.location.href = "chooseTime.html";
        sessionStorage.setItem('enteredTimes', JSON.stringify(enteredTimes));
        sessionStorage.setItem('enteredParticipants', JSON.stringify(enteredParticipants));
    });
    

    //When updating things in chooseTime.html
    $("input[name=update]").click(function () {
        // Get entered available times
        var times = document.getElementById("timeList");
        var timeListItem = times.getElementsByTagName("li");
        var enteredTimes = [];
        for (var i=0; i < timeListItem.length; i++) {
            var startEnd = timeListItem[i].getElementsByTagName( "select" );
            var startTime = startEnd[0].options[ startEnd[0].selectedIndex ].value;
            var endTime = startEnd[1].options[ startEnd[1].selectedIndex ].value;
            // Set start, end times
            if (startTime != "None") {
                if (endTime != "None") {
                    startTime = parseInt(startTime);
                    endTime = parseInt(endTime);
                    // If we have both start and ending times
                    if (startTime > endTime) {
                        alert("Starting time should be earlier than ending time for time " + (i+1) + "!");
                        return;
                    }
                    // Get weekday
                    var week = [];
                    var checked = times.getElementsByClassName("checkbox" + (i+1));
                    for (var j=0; j < checked.length; j++) {
                        if (checked[j].checked == true) {
                            week.push(checked[j].id);
                        }
                    }
                    var thisTime = {availWeek: week, startT: startTime, endT: endTime};
                    //alert(thisTime.availWeek.length);
                    enteredTimes.push(thisTime);
                } else {
                    // Only have starting time
                    alert("Please enter ending time for time " + (i+1) + "!");
                    return;
                }
            } else {
                if (endTime != "None") {
                    // Only have ending time
                    alert("Please enter starting time for time " + (i+1) + "!");
                    return;
                }
            }
        }
        if (enteredTimes.length == 0) {
            alert("You didn't choose anything!");
            return;
        }
        window.location.href = "chooseTime.html";
        sessionStorage.setItem('enteredTimes', JSON.stringify(enteredTimes));
    });

    // Remember previously selected values, only if we are at chooseTime.html
    var retrievedTimes = JSON.parse(sessionStorage.getItem('enteredTimes'));
    var currentPage = window.location.pathname;
    if (currentPage.indexOf("chooseTime.html", currentPage.length - "chooseTime.html".length) !== -1) {
        for (var i=0; i < retrievedTimes.length; i++) {
            var div = document.getElementById("time"+(i+1));
            var dropDowns = div.getElementsByTagName("select");
            // Set start, end times
            dropDowns[0].value = retrievedTimes[i].startT;
            dropDowns[1].value = retrievedTimes[i].endT;
            // Set weekdays
            var retrievedWeek = retrievedTimes[i].availWeek;
            var checked = div.getElementsByClassName("checkbox" + (i+1));
            for (var j=0; j < retrievedWeek.length; j++) {
                checked[weekdayArray[retrievedWeek[j]]].checked = true;
            }
        }

    }
    
    // Display selected times in chooseTime.html
    var retrievedParticipants = JSON.parse(sessionStorage.getItem('enteredParticipants'));
    var htmlContent = "";

    for (var i=0; i < retrievedParticipants.length; i++) {
        htmlContent += '<div id = "' + retrievedParticipants[i].thisId +'display">';
        htmlContent += 'Name: ' + retrievedParticipants[i].thisName + '<br>';
        htmlContent += 'City: ' + retrievedParticipants[i].thisCity + '<br>';
        htmlContent += 'Times: ' + '<br>';
        var diff = timeDiffs[retrievedParticipants[i].thisCity] - timeDiffs[retrievedParticipants[0].thisCity];
        //alert(retrievedParticipants[0].thisCity + " + " + diff + " = " + retrievedParticipants[i].thisCity);
        var newStart;
        var newEnd;
        var weekdayStr;
        for (var j=0; j < retrievedTimes.length; j++) {
            htmlContent += (j+1) + ': ';
            weekdays = retrievedTimes[j].availWeek;
            weekdayStr = "";

            newStart = parseInt(retrievedTimes[j].startT) + diff;

            // Can make more efficient!
            if (newStart > 23) {
                // Create newWeekdayStr
                if (weekdays.length > 0) {
                    weekdayStr += weekdayArray[(weekdayArray[weekdays[0]]+1+7)%7];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdayArray[(weekdayArray[weekdays[k]]+1+7)%7];
                }
                newStart = newStart%24;
            } else if (newStart < 1) {
                // Create newWeekdayStr
                if (weekdays.length > 0) {
                    weekdayStr += weekdayArray[(weekdayArray[weekdays[0]]-1+7)%7];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdayArray[(weekdayArray[weekdays[k]]-1+7)%7];
                }
                newStart = (newStart+24)%24;
            } else {
                if (weekdays.length > 0) {
                    weekdayStr += weekdays[0];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdays[k];
                }
            }
            if (newStart == 0) {
                newStart = "12AM";
            } else if (1 <= newStart && newStart <= 12) {
                newStart = newStart + "AM";
            } else {
                newStart = newStart%12 + "PM";
            }
            htmlContent += (newStart + ' (' + weekdayStr + ') --> ');


            newEnd = parseInt(retrievedTimes[j].endT) + diff;

            weekdayStr = "";
            if (newEnd > 23) {
                // Create newWeekdayStr
                if (weekdays.length > 0) {
                    weekdayStr += weekdayArray[(weekdayArray[weekdays[0]]+1+7)%7];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdayArray[(weekdayArray[weekdays[k]]+1+7)%7];
                }
                newEnd = newEnd%24;
            } else if (newEnd < 1) {
                // Create newWeekdayStr
                if (weekdays.length > 0) {
                    weekdayStr += weekdayArray[(weekdayArray[weekdays[0]]-1+7)%7];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdayArray[(weekdayArray[weekdays[k]]-1+7)%7];
                }
                newEnd = (newEnd+24)%24;
            } else {
                if (weekdays.length > 0) {
                    weekdayStr += weekdays[0];
                }
                for (var k = 1; k < weekdays.length; k++) {
                    weekdayStr += ", " + weekdays[k];
                }
            }
            if (newEnd == 0) {
                newEnd = "12AM";
            } else if (1 <= newEnd && newEnd <= 12) {
                newEnd = newEnd + "AM";
            } else {
                newEnd = newEnd%12 + "PM";
            }
            htmlContent += (newEnd + ' (' + weekdayStr + ')<br>');
        }
        htmlContent += '</div><hr>';
    }
    // Link to homepage
    htmlContent += '<form action="timeDiff.html"><input type="submit" name="homeButton" value="Homepage"></form>'
    
    document.querySelector('.results').innerHTML = htmlContent;//txt + txt2;
}; // end of main

$(document).ready(main);